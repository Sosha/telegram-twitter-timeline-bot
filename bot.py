#!/usr/bin/python3.5

from twitter import OAuth, TwitterStream
import telegram


telegram_bot = telegram.Bot("<Telegram API Token>") # Telegram API
auth = OAuth("<token>", "<token_secret>", "<consumer_key>", "<consumer_secret>") # Twitter API

twitter_userstream = TwitterStream(auth=auth, domain='userstream.twitter.com')
iterator = twitter_userstream.statuses.sample()


escapes = {
    '*': '\\*',
    '_': '\\_',
    '[': '\\[',
    '`': '\\`'
}


for msg in twitter_userstream.user():
    try:
        try:
            name = msg["user"]["name"] # Name of Tweeter
        except:
            name = "-"
        try:
            usr = msg["user"]["screen_name"] # UserName of Tweeter
        except:
            usr = "-"
        try:
            txt = msg["text"] # Text of the Tweet
        except:
            txt = "-"
        try:
            ftxt= msg["extended_tweet"]["full_text"] # Full Text of the Tweet
        except:
            ftxt = "-"
        try:
            mdurl = msg["extended_entities"]["media"][0]["media_url_https"] # Media URL
        except:
            mdurl = "-"
        try:
            idstr = msg["id_str"] # ID of the Tweet
        except:
            idstr = "-"
        try:
            retname = msg["retweeted_status"]["user"]["name"] # Name of Retweeter
        except:
            retname = "-"
        try:
            retusr = msg["retweeted_status"]["user"]["screen_name"] # UserName of Retweeter
        except:
            retusr = "-"
        try:
            rettxt = msg["retweeted_status"]["text"] # Text of the Retweet
        except:
            rettxt = "-"
        try:
            retftxt = msg["retweeted_status"]["extended_tweet"]["full_text"] # Full Text of the Retweet
        except:
            retftxt = "-"
        try:
            quotename = msg["quoted_status"]["user"]["name"] # Name of Quoter
        except:
            quotename = "-"
        try:
            quoteusr = msg["quoted_status"]["user"]["screen_name"] # UserName of Quoter
        except:
            quoteusr = "-"
        try:
            quoteid = msg["quoted_status"]["id_str"] # ID of the Quot
        except:
            quoteid = "-"
        try:
            quotetxt = msg["quoted_status"]["text"] # Text of the Quot
        except:
            quotetxt = "-"


        if ftxt != "-": # Replace Full Text instead Tweet Text
            txt = ftxt

        if retftxt != "-":
            rettxt = retftxt # Replace Full Text instead Retweet Text


        if name == "-" and usr == "-" and txt == "-" and mdurl == "-" and idstr == "-":
            print(msg, "\n")
            continue


        for i in escapes.keys():
            name = name.replace(i, escapes[i])
            txt = txt.replace(i, escapes[i])
            ftxt = ftxt.replace(i, escapes[i])
            retname = retname.replace(i, escapes[i])
            rettxt = rettxt.replace(i, escapes[i])
            retftxt = retftxt.replace(i, escapes[i])
            quotename = quotename.replace(i, escapes[i])
            quoteid = quoteid.replace(i, escapes[i])
            quotetxt = quotetxt.replace(i, escapes[i])
            mdurl = mdurl.replace(i, escapes[i])


        message_text = "*{name}* {usr}\n{retname}{retusr}{txt}\n{quotename}{quoteusr}{quoteid}{quotetxt}{mdurl}\n{idstr}".format(
            name=name,
            usr="[(@{}):]".format(usr) + "(https://twitter.com/" + usr + ")",
            retname=("🔃 ریتوییت کرده\n\n{}".format(retname) if retname != "-" else ""),
            retusr=" [(@{}):]".format(retusr) + "(https://twitter.com/" + retusr + ")" if retusr != "-" else "",
            txt=("\n{}".format(rettxt) if rettxt != "-" else "{}".format(txt if txt != "-" else "")),
            quotename = "\n{}".format(quotename) if quotename != "-" else "",
            quoteusr =" [(@{}):]".format(quoteusr) + "(https://twitter.com/" + quoteusr + ")" if quoteusr != "-" else "",
            quoteid =" [پیوند کوت]".format(quoteid) + "(https://twitter.com/" + quoteusr + "/status/" + quoteid + ")" if quoteid != "-" else "",
            quotetxt ="\n{}\n".format(quotetxt) if quotetxt != "-" else "",
            mdurl=("\n🖼 {}\n".format(mdurl) if mdurl != "-" else ""),
            idstr="[پیوند توییت](https://twitter.com/" + usr + "/status/" + "{}".format(idstr) + ")"
        )


        try:
            telegram_bot.sendMessage(<Your Telegram ID>, message_text, disable_web_page_preview=True, parse_mode=telegram.ParseMode.MARKDOWN)
        except Exception as e:
            print(message_text, "\n", e)


    except Exception as e:
        print("Error:\n", e)
